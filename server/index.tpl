<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <meta name="description" content="About Me">
    <meta name="author" content="">

   <!-- Mobile Specific Metas
   ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

   <!-- Add to home screen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="theme-color" content="#838C95">

  <!-- Add to home screen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <meta name="apple-mobile-web-app-title" content="Web Starter Kit">
  <meta name="apple-mobile-web-app-status-bar-style" content="#838C95">

  <!-- Tile icon for Win8 -->
  <meta name="msapplication-TileColor" content="#838C95">
  <meta name="msapplication-navbutton-color" content="#838C95">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:500" rel="stylesheet">


<title>Augmented Dictionary</title>
<style>
body{
    background-color: #4D5656;
    }/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
#image {
  height: 20px;
  width: 20px;
}
.mydiv {
    background-color:white;
}
/* Modal Content */
.modal-content {
    background-color: white;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
    font-family: 'Roboto',sans-serif;
}
#buton {
 width: 10px;
  height: 10px;
}
button.myBtn {background: none;border:none;color: white;font-family:'Montserrat',sans-serif;font-size: 1.5em;padding:auto;}
/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
.padd{padding:auto;}
</style>
</head>
<body>

<h2></h2>

<!-- Trigger/Open The Modal -->
<div class="padd">
% for i in xrange(len(ll)):
<button class="myBtn" id="{{i}}" onclick="foo('{{ll[i]}}','{{i}}')">{{ll[i]}}</button>
% end
</div>
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <div id="mypron" class ="mydiv"></div>
    <div id="mydef" class ="mydiv"></div>
    <div id="mysyn" class ="mydiv"></div>
  </div>
</div>

<script>
var modal = document.getElementById('myModal');
var btn = document.getElementsByTagName('button');
var span = document.getElementsByClassName("close")[0];

           var foo = function(word,word_id) {
                var XMLHttp = new XMLHttpRequest();
                
               var txt ="http://www.dictionaryapi.com/api/v1/references/collegiate/xml/"+word+"?key=2e2cff35-2462-4ae4-a9bc-978456888946";
               console.log(txt);
                XMLHttp.responseType = 'document';
                XMLHttp.overrideMimeType('text/xml');
        
                XMLHttp.onreadystatechange = function () {
                    if(this.readyState === 4) {
                        if(this.status ===200) {
                            myFunction(this,word_id);
                        }
                    }
                };
                 XMLHttp.open('GET',txt,true);
                XMLHttp.send();
            }
            function myFunction(xml,word_id) {
                parser = new DOMParser();
                mvar = new XMLSerializer();
                text = mvar.serializeToString(xml.response);
                xmlDoc = xml.responseXML;
                if (xmlDoc.getElementsByTagName("suggestion").length>0) {
                var mydef = document.getElementById("mydef");
                var mypron = document.getElementById("mypron");
                mypron.innerHTML = ""
                var mysyn = document.getElementById("mysyn");
                mysyn.innerHTML = ""
                mydef.innerHTML = "<h4>Word Not Found.<h4> <h3>Suggestion : </h3>"
                for (var k = 0; k <xmlDoc.getElementsByTagName("suggestion").length; k++) {
                    var newcontent = document.createElement('p');
                    newcontent.innerHTML = " " + (k+1) + ") " + xmlDoc.getElementsByTagName("suggestion")[k].childNodes[0].nodeValue;
                    mydef.appendChild(document.createElement('br'));    
                    mydef.appendChild(newcontent.firstChild);
                  } 
                }
                else{
                    var mydef = document.getElementById("mydef");
                    mydef.innerHTML = "<h3>Definitions:</h3>";
                    var count =1;
                    for (var k = 0; k <xmlDoc.getElementsByTagName("dt").length; k++) {
                        var newcontent = document.createElement('p');
                       if (xmlDoc.getElementsByTagName("dt")[k].childNodes[0].nodeValue != ':' && xmlDoc.getElementsByTagName("dt")[k].childNodes[0].nodeValue != null) {
                        newcontent.innerHTML = " " + (count++) + ") " + xmlDoc.getElementsByTagName("dt")[k].childNodes[0].nodeValue;
                        mydef.appendChild(document.createElement('br'));
                        mydef.appendChild(newcontent.firstChild);
                      }
                    }
                      if (count==1) {
                        var newcontent = document.createElement('p');
                        newcontent.innerHTML = "No Data Found"
                        mydef.appendChild(document.createElement('br'));
                        mydef.appendChild(newcontent.firstChild);
                      }
                    var mypron = document.getElementById("mypron");
                    mypron.innerHTML = "<h3>Pronunciation : </h3>";
                    var count=1;
                    for (var k = 0; k <xmlDoc.getElementsByTagName("pr").length; k++) {
                        var newcontent = document.createElement('p');
                        if (xmlDoc.getElementsByTagName("pr")[k].childNodes[0].nodeValue != ':' && xmlDoc.getElementsByTagName("pr")[k].childNodes[0].nodeValue != null){
                        newcontent.innerHTML = " " + (count++) + ") " + xmlDoc.getElementsByTagName("pr")[k].childNodes[0].nodeValue;
                        mypron.appendChild(document.createElement('br'));
                        mypron.appendChild(newcontent.firstChild);
                        }
                      }

                if (xmlDoc.getElementsByTagName("wav").length>0) {
                      var newcontent = document.createElement('section');
                      newcontent.innerHTML="<input type='image' src='http://img.freepik.com/free-icon/speaker-filled-audio-tool_318-75036.jpg?size=338&ext=jpg' id='image'>"

                      mypron.appendChild(document.createElement('br'));
                      mypron.appendChild(newcontent.firstChild);
                      var newcontent = document.getElementById('image')
                      newcontent.onclick = function()
                        {
                        var sound_name = xmlDoc.getElementsByTagName('wav')[0].childNodes[0].nodeValue;
                        var sound = new Audio("http://media.merriam-webster.com/soundc11/"+sound_name[0]+"/"+sound_name);
                        sound.play();
                        }
			}
                      if (count==1) {
                        mypron.innerHTML = "<h3>Pronunciation : </h3>";
                        var newcontent = document.createElement('p');
                        newcontent.innerHTML = "No Data Found"
                        mypron.appendChild(document.createElement('br'));
                        mypron.appendChild(newcontent.firstChild);
                      }

                      var mysyn = document.getElementById("mysyn");
                      mysyn.innerHTML = "<h3>Synonym : </h3>";
                    if (xmlDoc.getElementsByTagName('ss').length<1) {
                      var newcontent = document.createElement('p');
                      newcontent.innerHTML = "No Data Found";
                        mysyn.appendChild(document.createElement('br'));
                          mysyn.appendChild(newcontent.firstChild);
                    }
                    if (xmlDoc.getElementsByTagName('ss').length>0) {
                      var mysyn = document.getElementById("mysyn");
                      mysyn.innerHTML = "<h3>Synonym : </h3>";
                      var count=1;
                      for (var k = 0; k <xmlDoc.getElementsByTagName("ss").length; k++) {
                          var newcontent = document.createElement('p');
                          if (xmlDoc.getElementsByTagName("ss")[k].childNodes[0].nodeValue != ':' && xmlDoc.getElementsByTagName("ss")[k].childNodes[0].nodeValue != null){
                            var new_link = document.createElement('a');
                            console.log(word_id,xmlDoc.getElementsByTagName("ss")[k].childNodes[0].nodeValue);
                            // new_link.onclick = change(word_id,xmlDoc.getElementsByTagName("ss")[k].childNodes[0].nodeValue);

                          newcontent.innerHTML = " " + (count++) + ") " + xmlDoc.getElementsByTagName("ss")[k].childNodes[0].nodeValue;
                          mysyn.appendChild(new_link);
                          mysyn.appendChild(document.createElement('br'));
                          mysyn.appendChild(newcontent.firstChild);
                          }
                        }
                        if (count==1) {
                        var newcontent = document.createElement('p');
                        newcontent.innerHTML = "No Data Found"
                        mysyn.appendChild(document.createElement('br'));
                        mysyn.appendChild(newcontent.firstChild);
                         }

                    }
                }
                console.log(text);
                modal.style.display = "block";
                //alert(text);
            }
        // }
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";

}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

</body>
</html>
