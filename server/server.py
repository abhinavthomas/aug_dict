from bottle import get,post,request,Bottle,run,SimpleTemplate,template,response
import bottle
from PIL import Image
from io import BytesIO
import os
from base64 import *
from tesserocr import *
import re
import json
import random
import string
app=Bottle()
bottle.BaseRequest.MEMFILE_MAX = 1024 * 1024 *1024

def function(name):
    with PyTessBaseAPI() as api:
        api.SetImageFile('temp/'+name)
        return (api.GetUTF8Text(),api.AllWordConfidences())

@app.get('/')
def foo():
    return '''
        <form action="/temp" method="post" enctype="multipart/form-data">
          Select a file: <input type="file" name="upload" />
          <input type="submit" value="Start upload" />
        </form>
    '''
@app.post('/temp')
def temp():
    upload = request.files.get('upload')

    print upload.filename
    print type(upload.file)
    name, ext = os.path.splitext(upload.filename)

    if ext not in ('.png','.jpg','.jpeg','.gif','.PNG','.JPG','.JPEG','.GIF'):
       return 'File extension not allowed.'

    save_path = 'temp/'

    try:
        upload.save(save_path) # appends upload.filename automatically
        print "saved"

    except IOError as e:
        os.remove(save_path+upload.filename)
        print "successfull"
        upload.save(save_path) # appends upload.filename automatically

    finally:
        data = function(upload.filename)
        text = data[0]
        print text
        li = text.strip().split()
        if not(len(li)):
            li.extend('Oops no text found'.split())
        li = [re.sub('\W+','',i) for i in li]
        return template('index',ll=li)

@app.hook('after_request')
def enable_cors():
    print "after_request hook"
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

@app.route('/', method=['OPTIONS', 'POST'])
def i_p():
    if request.method == 'OPTIONS':
        return {}
    else:
        data = request.json
        print data
        print type(data['upload'].encode('utf-8'))
        image = data['upload']
        name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(100))
        name+='.jpeg'
        with open('temp/'+name, "wb") as fh:
            fh.write(image.decode('base64'))
        data = function(name)
        text = data[0]
        print text
        li = text.strip().split()
        if not(len(li)):
            li.extend('Oops no text found'.split())
        li = [re.sub('\W+','',i) for i in li]
    return template('index',ll=li)

if __name__ == '__main__':
    run(app,host='0.0.0.0', port=8080)
