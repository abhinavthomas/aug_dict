# Augmented Disctionary#

Even after massive developments in technology, we have not been able to overcome language barriers. Humans still find it difficult to effectively
understand the meaning of different words which they encounter in their daily life. We aim to find a solution to this problem by developing a
convenient, live, platform-independent dictionary which alleviates human effort.
The project is to develop an application which will read the text from images and let the user select one of the words and obtain its meaning.

Work flow:

![Flow Diagram](./images/flow.png)

* The image is received at the server.

* Image is provided to Tesseract library and the Google Cloud Vision API.

* Image is processed in Tesseract library and the text is returned.

* Cloud Vision API returns text to train the Tesseract model using tensorflow.

Results:
1. ![Flow Diagram](./images/1.png)
2. ![Flow Diagram](./images/2.png)
3. ![Flow Diagram](./images/3.png)
4. ![Flow Diagram](./images/4.png)
5. ![Flow Diagram](./images/5.png)
6. ![Flow Diagram](./images/6.png)
